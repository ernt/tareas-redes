---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Crear merge request
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Crear _merge request_

## Crea un _merge request_ para entregar tu actividad

Para integrar las tareas de todos se utilizará la funcionalidad `merge request` de GitLab

- Accede a la url que te apareció en el paso anterior

```text
https://gitlab.com/USUARIO/tareas-redes/-/merge_requests/new?merge_request%5Bsource_branch%5D=NOMBRE
```

- Verifica que la rama de destino sea la rama de la actividad que vas a entregar (`tarea-1`, `practica-2`, etc.)
    - Da clic en el botón **Change branches** para ajustar si es necesario

| ![](img/009-Fork-MR_change-branches.png)
|:---------------------------------------:|
|

- Cambia a la rama de la actividad que vas a entregar (`tarea-1`, `practica-2`, etc.)

| ![](img/009-Fork-MR_select-branch.png)
|:-------------------------------------:|
|

- Llena los datos del _merge request_
    - Escribe un título y una descripción que sirva como vista previa para tu entrega
- Da clic en el botón _submit_ para crear tu _merge request_

!!! warning
    Asegúrate de dejar las siguientes casillas <span class="red">sin marcar</span>

    -  `[ ]` _Delete source branch when merge request is accepted_
    -  `[ ]` _Squash commits when merge request is accepted_

| ![](img/009-Fork-MR_data.png)
|:-----------------------------:|
|

### Estado del pipeline

Este [repositorio de tareas][repositorio-tareas] cuenta con una configuración de CI/CD que publica el contenido de la carpeta `docs` en [un sitio web][repositorio-vista-web].
Esta operación se realiza mediante un _pipeline_ que construye el sitio y que puede tener uno de 4 estados

| Estado                                        | Acción
|:---------------------------------------------:|:-----------------------------|
| <span class="gold text-border">Pending</span> | Esperar a que el _pipeline_ se ejecute
| <span class="blue">Running</span>             | Esperar a que el _pipeline_ termine su ejecución
| <span class="green">Passed</span>             | El trabajo de CI/CD fue exitoso y el _merge request_ podrá ser revisado y aprobado
| <span class="red">Failed</span>               | Revisar los mensajes de la bitácora y corregir errores

!!! note
    - Normalmente los errores del pipeline se dan por archivos que se referencían pero no existen
    - Verificar los nombres de archivo y las ligas a imágenes y archivos complementarios
    - Para más información consulta la sección de [problemas comúnes](debug.md)

--------------------------------------------------------------------------------

##### Notificaciones de creación y seguimiento del _merge request_

- Una vez que hayas enviado el `merge request`, le llegará un correo electrónico al responsable para que integre tus cambios y podrás visualizar la revisión en la liga
    - <https://gitlab.com/Redes-Ciencias-UNAM/2023-1/tareas-redes/-/merge_requests>

<!--
| ![](img/010-Main_MR_created.png)
|:--------------------------------:|
|
-->

- Cuando se hayan integrado tus cambios te llegará un correo electrónico de confirmación y aparecerá en el panel del [repositorio principal][repositorio-tareas]

| ![](img/012-Main_MR_merged.png)
|:-------------------------------:|
|


--------------------------------------------------------------------------------

!!! note
    - Regresa a [la página principal][arriba] cuando hayas agregado el contenido a tu _fork_ y enviado los cambios a tu repositorio en GitLab

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) |                               |

[anterior]: ../configurar-precommit
[arriba]: ../../workflow
[siguiente]: ../../workflow
